package com.mygdx.game.view.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.mygdx.game.stors.ObjectStore;
import com.mygdx.game.utils.ObjectManager;

/**
 * Created by Home-Sweet-Home on 09.07.2017.
 */
public class GameScreen implements Screen {

    private final SpriteBatch spriteBatch;
    private World world;
    private OrthographicCamera camera;
    private ExtendViewport viewport;
    private Box2DDebugRenderer renderer;
    int x=800, y=800;
    float  accumulator = 0;
    static final float STEP_TIME = 1f / 60f;
    static final int VELOCITY_ITERATIONS = 6;
    static final int POSITION_ITERATIONS = 2;

    public GameScreen(){
        world = new World(new Vector2(0f,-90.8f),true);//-9.8f
        camera = new OrthographicCamera();
        //camera.setToOrtho(false,x,y);
        viewport = new ExtendViewport(x, y, camera);
        renderer = new Box2DDebugRenderer();
        spriteBatch = new SpriteBatch();
        ObjectManager.getInstance().setWorld(world);
        ObjectManager.getInstance().createAllGameObject();
    }

    @Override
    public void show() {

    }

    private void stepWorld() {
        float delta = Gdx.graphics.getDeltaTime();

        accumulator += Math.min(delta, 0.25f);

        if (accumulator >= STEP_TIME) {
            accumulator -= STEP_TIME;
            world.step(STEP_TIME, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
        }
    }

    @Override
    public void render(float delta) {
        ObjectManager.getInstance().removeMarkedObjects();
        ObjectManager.getInstance().addMarkedObjects();
        //Gdx.gl.glClearColor(0.57f, 0.77f, 0.85f, 1);
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        //camera.setToOrtho(true, x*delta,y*delta);
        //camera. . apply(Gdx.gl10);
        stepWorld();
        ObjectManager.getInstance().runAllLogicalProcess(delta);
        spriteBatch.begin();
        ObjectManager.getInstance().drawAllSprites(spriteBatch);
        spriteBatch.end();
        renderer.render(world, camera.combined);
    }
    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);

        spriteBatch.setProjectionMatrix(camera.combined);
        //camera.position.add(i,0,i);
//        i++;
        //camera.setToOrtho(false,300*i,300*i);
////        float aspectRatio = (float) width / (float) height;
////        camera = new PerspectiveCamera(64, x * aspectRatio, y);
////        Matrix4 viewMatrix = new Matrix4();
////        viewMatrix.setToOrtho2D(0, 0,width, height);
////        spriteBatch.setProjectionMatrix(viewMatrix);
//        float aspectRatio = (float) width / (float) height;
//        camera = new OrthographicCamera(x * aspectRatio, y);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
