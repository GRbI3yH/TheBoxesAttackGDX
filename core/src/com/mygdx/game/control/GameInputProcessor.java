package com.mygdx.game.control;

import com.badlogic.gdx.InputProcessor;
import com.mygdx.game.model.Player;

/**
 * Created by Home-Sweet-Home on 09.07.2017.
 */
public class GameInputProcessor implements InputProcessor {

    public GameInputProcessor (Player player){
        this.player = player;
    }

    private Player player;

    @Override
    public boolean keyDown(int keycode) {
        player.keyPress(keycode);
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        player.keyRelease(keycode);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
