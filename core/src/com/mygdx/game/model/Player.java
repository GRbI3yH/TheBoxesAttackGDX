package com.mygdx.game.model;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.mygdx.game.utils.MyMassData;
import com.mygdx.game.utils.PoolResources;

/**
 * Created by Home-Sweet-Home on 13.07.2017.
 */
public class Player extends GameObject  {

    private boolean bocKey;
    private boolean leftPressed,rightPressed,upPressed;
    private boolean isBubble1Active;
    private Sprite spriteSquirrelLeft, spriteSquirrelСenterж, spriteSquirrelRight ;
    private boolean isOnGround = false;
    private float travelSpeed = 100f;

    public Player(Body body) {
        super(PoolResources.getInstance().getTexture("Squirrel"), body,new MyMassData(0,0));
        spriteSquirrelСenterж = sprite;

        Texture textureSquirel = PoolResources.getInstance().getTexture("SquirrelMoves");
        textureSquirel.setFilter(Texture.TextureFilter.Linear,Texture.TextureFilter.Linear);
        spriteSquirrelLeft = new Sprite(textureSquirel);
        spriteSquirrelRight = new Sprite(textureSquirel);
        spriteSquirrelRight.flip(true,false);
    }

    @Override
    public void dispose() {
        destroy();
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        drawSprite(spriteBatch);
    }

    @Override
    public void logicalProcess(float dt) {
        if (leftPressed){
            //body.applyLinearImpulse(new Vector2(-1,0),new Vector2(-10,0),true);
            body.applyForceToCenter(-travelSpeed, 0.0f, true);
            if (spriteSquirrelLeft.hashCode() != sprite.hashCode()){
                sprite = spriteSquirrelLeft;
            }
        }else if (rightPressed){
            //body.applyLinearImpulse(new Vector2(1,0),new Vector2(10,0),true);
            body.applyForceToCenter(travelSpeed, 0.0f, true);
            if (spriteSquirrelRight.hashCode() != sprite.hashCode()){
                sprite = spriteSquirrelRight;
            }
        }else {
            if (spriteSquirrelСenterж.hashCode() != sprite.hashCode()){
                sprite = spriteSquirrelСenterж;
            }
        }
        if (upPressed){
            if (isOnGround){
                //body.applyLinearImpulse(new Vector2(0,100),new Vector2(0,0),true);
                body.applyForceToCenter(0.0f, travelSpeed*10.0f, false);
            }

         }
        if(isBubble1Active){
            body.setTransform(300,300,0);
        }

    }

    private void setLeftPressed(boolean leftPressed) {
        if (!bocKey){
            this.leftPressed = leftPressed;
        }
    }

    private void setRightPressed(boolean rightPressed) {
        if (!bocKey){
            this.rightPressed = rightPressed;
        }
    }

    private void setUpPressed(boolean upPressed) {
        if (!bocKey){
            this.upPressed = upPressed;
//            ISprite sprite = SpritesStore.getInstance().CollisionCheck(Player.this, new Vector2(0,1));
//            if (sprite!=null) {
//                jumpB = true;
//                jump = jumpStrength;
//            }
        }
    }

    private void activeBonus(boolean upPressed) {
//        if ((isBubble1Active == false)&&(numberOfBubbles > 0) ) {
            isBubble1Active = upPressed;
//            BubbleTimeLife = 100;
//            numberOfBubbles--;
//        }
    }

    public void setIsOnGround(boolean is){
        isOnGround = is;
    }

    public void keyRelease(int key){
        boolean b = false;
        if((key == Input.Keys.W) || (key == Input.Keys.UP)){
            setUpPressed(b);
        }else if(key == Input.Keys.SPACE){
            activeBonus(b);
        }
        else if((key == Input.Keys.D)||(key == Input.Keys.RIGHT)){
            setRightPressed(b);
        }
        else if((key == Input.Keys.A)||(key == Input.Keys.LEFT)){
            setLeftPressed(b);
        }
    }

    public void keyPress(int key){
        boolean b = true;
        if((key == Input.Keys.W) || (key == Input.Keys.UP)){
            setUpPressed(b);
        }else if(key == Input.Keys.SPACE){
            activeBonus(b);
        }
        else if((key == Input.Keys.D)||(key == Input.Keys.RIGHT)){
            setRightPressed(b);
        }
        else if((key == Input.Keys.A)||(key == Input.Keys.LEFT)){
            setLeftPressed(b);
        }
    }



    @Override
    public Body getBody() {
        return body;
    }

    @Override
    void initBody() {
        CircleShape circleShape = new CircleShape();
        PolygonShape shape = new PolygonShape();
        FixtureDef fixtureDef = new FixtureDef();

        circleShape.setPosition(new Vector2(0,-16));
        circleShape.setRadius(16);
        fixtureDef.friction = 0;
        fixtureDef.isSensor = false;
        fixtureDef.shape = circleShape;
        body.createFixture(fixtureDef).setUserData(this);


        shape.setAsBox(fixPosW/1.5f,fixPosH/1.2F);
        fixtureDef.shape = shape;
        fixtureDef.friction = 0;
        fixtureDef.isSensor = false;
        body.createFixture(fixtureDef).setUserData(this);


        shape.setAsBox(fixPosW/1.6f,fixPosH/4F,new Vector2(0,-35),0);
        fixtureDef.shape = shape;
        fixtureDef.friction = 0;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData("foot");

        shape.dispose();
        circleShape.dispose();

    }
}
