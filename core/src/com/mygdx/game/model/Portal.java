package com.mygdx.game.model;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.mygdx.game.utils.*;

/**
 * Created by Home-Sweet-Home on 29.07.2017.
 */
public class Portal extends GameObject {

    private com.mygdx.game.utils.Animator animator;
    private int vector;
    int i = 0;

    public Portal(Body body, int vector) {
        super(body,32/2,32/2,new MassData());
        animator = new com.mygdx.game.utils.Animator(PoolResources.getInstance().getTextureRegion("PortalHole"),0.08f);
        this.vector = vector;
    }

    @Override
    public void dispose() {
        destroy();
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(animator.getKeyFrame(true), body.getTransform().getPosition().x-fixPosW, body.getTransform().getPosition().y-fixPosH);
    }

    @Override
    public void logicalProcess(float dt) {
        if(i > 30){
        //    new Thread(new Runnable() {
        //        @Override
        //        public void run() {
        //            Gdx.app.postRunnable(new Runnable() {
        //                @Override
        //                public void run() {
                            IGameObject iGameObject;
                            if (Math.random() < 0.5) {
                                iGameObject = ObjectManager.getInstance().createBox(body.getTransform().getPosition().x,body.getTransform().getPosition().y);
                            }else {
                                iGameObject = ObjectManager.getInstance().createRock(body.getTransform().getPosition().x,body.getTransform().getPosition().y);
                            }

                            float randomX = (float) Math.random()*6000.0f;
                            float randomY = (float) Math.random()*5000.0f;
                            if (Math.random() < 0.5){
                                iGameObject.getBody().applyForceToCenter((randomX * -1.0f), randomY, true);
                            }else {
                                iGameObject.getBody().applyForceToCenter((randomX * 1.0f), randomY, true);
                            }

                            ObjectManager.getInstance().addObject(iGameObject);
                            i=0;
                            if (Math.random() < 0.5) {
                                body.setTransform(new Vector2((float) (Math.random()*800),800-32),0);
                            }
        //                }
        //            });
        //        }
        //    }).start();
        }
        i++;
    }

    @Override
    void initBody() {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(fixPosW,fixPosH);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.friction = 1;
        fixtureDef.isSensor = true;
        fixtureDef.shape = shape;

        body.createFixture(fixtureDef).setUserData(this);

        shape.dispose();
    }

    @Override
    public Body getBody() {
        return body;
    }
}
