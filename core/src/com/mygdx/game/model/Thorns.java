package com.mygdx.game.model;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.*;
import com.mygdx.game.utils.PoolResources;


/**
 * Created by Home-Sweet-Home on 29.07.2017.
 */
public class Thorns extends GameObject {

    private com.mygdx.game.utils.Animator animator;

    public Thorns(Body body) {
        super(body,512/2,32/2,new MassData());
        animator = new com.mygdx.game.utils.Animator(PoolResources.getInstance().getTextureRegion("Thorns"),0.08f);
    }

    @Override
    public void dispose() {
        destroy();
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(animator.getKeyFrame(true), body.getTransform().getPosition().x-fixPosW, body.getTransform().getPosition().y-fixPosH);
    }

    @Override
    public void logicalProcess(float dt) {

    }

    @Override
    void initBody() {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(fixPosW,fixPosH);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.friction = 1;
        fixtureDef.isSensor = false;
        fixtureDef.shape = shape;

        body.createFixture(fixtureDef).setUserData(this);

        shape.dispose();
    }

    @Override
    public Body getBody() {
        return body;
    }

}
