package com.mygdx.game.model;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Transform;
import com.mygdx.game.utils.Animator;
import com.mygdx.game.utils.ObjectManager;

/**
 * Created by Home-Sweet-Home on 06.08.2017.
 */
public class Destruction implements IGameObject {

    private Animator animator;
    private Transform pos;

    public Destruction(Animator animator){
        this.animator = animator;
    }

    public void startDestruction(Transform pos){
        ObjectManager.getInstance().addObject(this);
        this.pos = pos;
    }

    @Override
    public void dispose() {
        ObjectManager.getInstance().destroyObject(this);
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        if( ! animator.isAnimationFinished() ){
            dispose();
        }
        spriteBatch.draw(animator.getKeyFrame(false), pos.getPosition().x, pos.getPosition().y);

    }

    @Override
    public void logicalProcess(float dt) {

    }

    @Override
    public Body getBody() {
        return null;
    }
}
