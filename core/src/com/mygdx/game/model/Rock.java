package com.mygdx.game.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.*;
import com.mygdx.game.utils.Animator;
import com.mygdx.game.utils.MyMassData;
import com.mygdx.game.utils.PoolResources;

/**
 * Created by Home-Sweet-Home on 29.07.2017.
 */
public class Rock extends GameObject {

    private Animator animator;
    private boolean destroy = false,runAnimate = false;
    private int lifs = 600;

    public Rock(Body body) {
        super(PoolResources.getInstance().getTexture("Rock"), body, new MyMassData(1000,10));
        animator = new Animator(PoolResources.getInstance().getTextureRegion("RockCrash"),0.08f);
    }

    @Override
    public void dispose() {
        if(destroy == false) destroy = true;
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        if (runAnimate == true){
            //System.out.println(animator.isAnimationFinished());
            if(animator.isAnimationFinished()){
                destroy();
            }else {
                spriteBatch.draw(animator.getKeyFrame(false),body.getTransform().getPosition().x-32,body.getTransform().getPosition().y-32);
            }
        }else {
            drawSprite(spriteBatch);
        }
    }

    @Override
    public void logicalProcess(float dt) {
        if(destroy){
            lifs--;
            if(lifs<100){
                if (lifs % 2 == 0){
                    sprite.setColor(Color.CLEAR);//эта хрень оставляет много мусорного колора
                }else {
                    sprite.setColor(Color.RED);//эта хрень оставляет много мусорного колора
                }
                if(lifs<0){
                    runAnimate = true;
                }
            }
        }
    }

    @Override
    void initBody() {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(fixPosW,fixPosH);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.friction = 1;
        fixtureDef.isSensor = false;
        fixtureDef.shape = shape;

        body.createFixture(fixtureDef).setUserData(this);

        shape.dispose();
    }

    @Override
    public Body getBody() {
        return body;
    }
}
