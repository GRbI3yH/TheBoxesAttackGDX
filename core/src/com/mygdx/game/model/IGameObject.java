package com.mygdx.game.model;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.ContactListener;

/**
 * Created by Home-Sweet-Home on 20.07.2017.
 */
public interface IGameObject {
    public void dispose();
    public void draw(SpriteBatch spriteBatch);
    public void logicalProcess(float dt);
    public Body getBody();
}
