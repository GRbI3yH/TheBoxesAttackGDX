package com.mygdx.game.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.*;
import com.mygdx.game.utils.ObjectManager;

/**
 * Created by Home-Sweet-Home on 09.07.2017.
 */
public abstract class GameObject implements IGameObject {

    protected Body body;
    protected Sprite sprite;
    protected final float fixPosW, fixPosH;

    public GameObject(Texture texture, Body body,MassData massData){
        body.setMassData(massData);
        this.body = body;
        texture.setFilter(Texture.TextureFilter.Linear,Texture.TextureFilter.Linear);
        sprite = new Sprite(texture);
        sprite.setSize(sprite.getWidth(),sprite.getHeight());
//        sprite.setCenter(sprite.getWidth()/2,sprite.getHeight()/2);
        //sprite.setCenter(sprite.getWidth()/2,sprite.getHeight()/2);
        //sprite.setOrigin(sprite.getWidth()/2,sprite.getHeight()/2);
        fixPosW = sprite.getWidth()/2;
        fixPosH = sprite.getHeight()/2;
        //sprite.setOrigin(sprite.getX()/2,sprite.getY()/2);
        initBody();
    }

    public GameObject(Body body,int fixPosW,int fixPosH,MassData massData){
        body.setMassData(massData);
        this.body = body;
        this.fixPosW = fixPosW;
        this.fixPosH = fixPosH;
        initBody();
    }

    protected void drawSprite(SpriteBatch spriteBatch){
        if(body != null || sprite != null ){
            sprite.setPosition(body.getPosition().x-fixPosW,body.getPosition().y-fixPosH);
            sprite.setOrigin(sprite.getWidth()/2,sprite.getHeight()/2);
            sprite.setRotation((float) Math.toDegrees(body.getAngle()));
            sprite.draw(spriteBatch);
        }

    }

    abstract void initBody();

    protected void destroy(){
        if (body != null || sprite != null){
            ObjectManager.getInstance().destroyObject(this);
//            sprite = null;
//            body=null;
        }
    }

    protected void destroy(Destruction destruction){
        if (body != null || sprite != null){
            destruction.startDestruction(body.getTransform());
            ObjectManager.getInstance().destroyObject(this);
//            sprite = null;
//            body=null;
        }
    }

}