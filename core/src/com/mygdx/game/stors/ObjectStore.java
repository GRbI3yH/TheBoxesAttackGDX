package com.mygdx.game.stors;



import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.model.IGameObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/**
 * Created by Home on 22.02.2017.
 */

public class ObjectStore {

    private List<IGameObject> sprites = Collections.synchronizedList(new ArrayList<IGameObject>());

    private IGameObject getSprite(int i) {
        return sprites.get(i);
    }

    public synchronized void addSprite (IGameObject... sprites){
        synchronized (sprites){
            for (IGameObject sprite:sprites){
                this.sprites.add(sprite);
            }
        }
    }

    public synchronized void  remove(IGameObject sprite){
        List<IGameObject> sprites = new ArrayList<IGameObject>();
        for (IGameObject listener:this.sprites) {
            sprites.add(listener);
        }
        sprites.remove(sprite);
        this.sprites = sprites;
        sprites = null;
        
    }

    public synchronized void dispose(){
        for (IGameObject listener:this.sprites) {
            listener.dispose();
        }
    }

    public synchronized void clearListeners(){
        sprites.clear();
    }

    public synchronized void drawAllSprites(SpriteBatch spriteBatch){
        synchronized (sprites){
            Iterator<IGameObject> iterator = sprites.iterator();
            while (iterator.hasNext()){
                IGameObject sprite = iterator.next();
                if(sprite != null){
                    sprite.draw(spriteBatch);
                }
            }
        }
    }

    public synchronized void runAllLogicalProcess(float dt){
        synchronized (sprites){
            Iterator<IGameObject> iterator = sprites.iterator();
            while (iterator.hasNext()){
                IGameObject sprites = iterator.next();
                if(sprites != null){
                    sprites.logicalProcess(dt);
                }
            }
        }
    }
}
