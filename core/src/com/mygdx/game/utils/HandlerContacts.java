package com.mygdx.game.utils;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.physics.box2d.*;
import com.mygdx.game.model.*;

/**
 * Created by Home-Sweet-Home on 09.08.2017.
 */
public class HandlerContacts implements ContactListener {

    private Player player;

    public HandlerContacts(Player player){
        this.player = player;
    }

    @Override
    public void beginContact(Contact contact) {
        //if (b) {
            //b =false ;
            //System.out.println("От сюда" + contact.getFixtureA().getUserData()+" есть контакт "+contact.getFixtureB().getUserData());
        if(checTheNull(contact)) return;

        if(contact.getFixtureA().getUserData().equals("foot") || contact.getFixtureB().getUserData().equals("foot")){
            if(Thorns.class != contact.getFixtureA().getUserData().getClass()&&Thorns.class != contact.getFixtureB().getUserData().getClass()){
                player.setIsOnGround(true);
            }
        }

        if(Thorns.class == contact.getFixtureA().getUserData().getClass()){
            IGameObject  iGameObject = (IGameObject) contact.getFixtureB().getUserData();

//            if (iGameObject.getClass() == Box.class){
//                Sound sound = PoolResources.getInstance().getSound("LandingBox");
//                long id = sound.play(0.3f);
//            }else if (iGameObject.getClass() == Rock.class){
//                Sound sound = PoolResources.getInstance().getSound("LandingRock");
//                long id = sound.play(0.3f);
//            }
            // play new sound and keep handle for further manipulation
//            sound.stop(id);             // stops the sound instance immediately
//            sound.setPitch(id, 2);      // increases the pitch to 2x the original pitch
//
//            id = sound.play(1.0f);      // plays the sound a second time, this is treated as a different instance
//            sound.setPan(id, -1, 1);    // sets the pan of the sound to the left side at full volume
//            sound.setLooping(id, true); // keeps the sound looping
//            sound.stop(id);             // stops the looping sound

            iGameObject.dispose();
            return;
        }else if (Thorns.class == contact.getFixtureB().getUserData().getClass()){
            if(contact.getFixtureA().getUserData().getClass() != String.class){
                IGameObject  iGameObject = (IGameObject) contact.getFixtureA().getUserData();
                iGameObject.dispose();
            }
        }
//        if (Thorns.class == contact.getFixtureB().getUserData().getClass()){
//            b++;
//            //System.out.println("ага");
            //new Destruction(new Animator("boxCrash.png",3,1,900f),body.getTransform());
//        }
            //ContactFilter
        //}
    }

    @Override
    public void endContact(Contact contact) {
        if(checTheNull(contact)) return;
        if(contact.getFixtureA().getUserData().equals("foot") || contact.getFixtureB().getUserData().equals("foot")){
            player.setIsOnGround(false);
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    public boolean checTheNull(Contact contact){
        if(player == null)return true;
        if(contact.getFixtureA() == null || contact.getFixtureB() == null) return true;
        if(contact.getFixtureA().getUserData() == null || contact.getFixtureB().getUserData() == null) return true;
        return false;
    }

}
