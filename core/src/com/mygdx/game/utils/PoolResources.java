package com.mygdx.game.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Home-Sweet-Home on 20.08.2017.
 */
public class PoolResources {

    private Map<String,Sound> mapListAudio = new HashMap<String,Sound>();
    private Map<String,Texture> mapListTexture = new HashMap<String,Texture>();
    private Map<String,TextureRegion[]> mapTextureRegion = new HashMap<String, TextureRegion[]>();

    private static PoolResources ourInstance = new PoolResources();
    public static PoolResources getInstance() {
        return ourInstance;
    }

    private PoolResources(){
        mapListTexture.put("background",new Texture("BacG2.png"));
        mapListTexture.put("SquirrelMoves",new Texture("SquirrelMoves.png"));
        mapListTexture.put("Squirrel",new Texture("Squirrel.png"));
        mapListTexture.put("box",new Texture("box.png"));
        mapListTexture.put("Rock",new Texture("Rock.png"));

        mapListAudio.put("LandingBox", Gdx.audio.newSound(Gdx.files.internal("LandingBox.mp3")));
        mapListAudio.put("LandingRock", Gdx.audio.newSound(Gdx.files.internal("LandingRock.mp3")));

        mapTextureRegion.put("PortalHole",textureToTextureRegion(new Texture("PortalHole.png"),2, 2));
        mapTextureRegion.put("RockCrash",textureToTextureRegion(new Texture("RockCrash.png"),3,1));
        mapTextureRegion.put("boxCrash",textureToTextureRegion(new Texture("boxCrash.png"),3,1));
        mapTextureRegion.put("Thorns",textureToTextureRegion(new Texture("Thorns.png"),1, 2));
    }

    public Sound getSound(String resource){
        Sound res = null;
        if(resource != null){
           res = mapListAudio.get(resource);
        }
        return res;
    }

    public Texture getTexture(String resource){
        Texture res = null;
        if(resource != null){
            res = mapListTexture.get(resource);
        }
        return res;
    }

    public TextureRegion[] getTextureRegion(String resource){
        TextureRegion[] res = null;
        if(resource != null){
            res = mapTextureRegion.get(resource);
        }
        return res;
    }

    private TextureRegion[] textureToTextureRegion(Texture img,int FRAME_COLS,int FRAME_ROWS){
        Texture walkSheet = img;
        TextureRegion[][] tmp = TextureRegion.split(walkSheet,
                walkSheet.getWidth() / FRAME_COLS,
                walkSheet.getHeight() / FRAME_ROWS);

        TextureRegion[] fragments = new TextureRegion[FRAME_ROWS*FRAME_COLS];
        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                fragments[index++] = tmp[i][j];
            }
        }
        return fragments;
    }

    public void dispose(){
        mapListAudio.clear();
        mapListTexture.clear();
        mapTextureRegion.clear();
    }
}
