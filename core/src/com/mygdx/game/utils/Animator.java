package com.mygdx.game.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Home-Sweet-Home on 04.08.2017.
 */
public class Animator {
    private float stateTime = 0f;
    private Animation<TextureRegion> animation;
    //float animatSpeed = 0;
    public Animator(TextureRegion[] textureRegion,float animateSpeed){
        animation = new Animation<TextureRegion>(animateSpeed, textureRegion);
    }

    public TextureRegion getKeyFrame(boolean looping){
        stateTime += Gdx.graphics.getDeltaTime();
        return animation.getKeyFrame(stateTime, looping);
    }

    public boolean isAnimationFinished(){
        return animation.isAnimationFinished(stateTime);
    }
}
