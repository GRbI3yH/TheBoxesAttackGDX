package com.mygdx.game.utils;

import com.badlogic.gdx.physics.box2d.MassData;

/**
 * Created by Home-Sweet-Home on 06.08.2017.
 */
public class MyMassData extends MassData {
    public MyMassData(float I, float mass){
        this.I = I;
        this.mass = mass;
    }
}
