package com.mygdx.game.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.mygdx.game.control.GameInputProcessor;
import com.mygdx.game.model.*;
import com.mygdx.game.stors.ObjectStore;

import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Home-Sweet-Home on 13.07.2017.
 */
public class ObjectManager {

    private ObjectStore objectStore = new ObjectStore();
    private ArrayList<IGameObject> markedForDeletion = new ArrayList<IGameObject>();
    private ArrayList<IGameObject> markedForAdd = new ArrayList<IGameObject>();
    private Texture textureBackgraund = new Texture("BacG2.png");

    public void setWorld(World world) {
        this.world = world;
    }

    private World world;
    private static ObjectManager ourInstance = new ObjectManager();
    public synchronized static ObjectManager getInstance() {
        return ourInstance;
    }
    private ObjectManager(){

    }

    public void runAllLogicalProcess(float delta){
        objectStore.runAllLogicalProcess(delta);
    }

    public void drawAllSprites(SpriteBatch spriteBatch){
        spriteBatch.draw(textureBackgraund,-200,-200);
        objectStore.drawAllSprites(spriteBatch);
    }

    public void createAllGameObject(){
        List<IGameObject> iGameObjects = new ArrayList<IGameObject>();

        /** add more Object */
        for (int i = 0;i<5;i++){
            iGameObjects.add(createRock((float) (Math.random()*800),100f));
        }
        Player player = new Player(createBodyObject(BodyDef.BodyType.DynamicBody,(float) (Math.random()*800),500f,0));
        HandlerContacts handlerContacts = new HandlerContacts(player);
        signObjectЕoListeners(handlerContacts);


        GameInputProcessor gameInputProcessor = new GameInputProcessor(player);
        Gdx.input.setInputProcessor(gameInputProcessor);

        iGameObjects.add(player);
        iGameObjects.add(new Thorns(createBodyObject(BodyDef.BodyType.StaticBody,512/2,16,0)));
        iGameObjects.add(new Thorns(createBodyObject(BodyDef.BodyType.StaticBody,((512/2)+512),16,0f)));

        iGameObjects.add(new Portal(createBodyObject(BodyDef.BodyType.StaticBody,800-32,800-32,0),-1));
        /** end add Object */

        IGameObject[] iGameObjects1array = iGameObjects.toArray(new  IGameObject[iGameObjects.size()]);
        objectStore.addSprite(iGameObjects1array);
        createBorderWorld();
        Music mp3Music = Gdx.audio.newMusic(Gdx.files.internal("Embient.mp3"));
        mp3Music.play();
        mp3Music.setVolume(0.1f);
        mp3Music.setLooping(true);
    }

    public Box createBox(float x, float y){
        return new Box(createBodyObject(BodyDef.BodyType.DynamicBody,x,y,0));
    }

    public Rock createRock(float x, float y){
        return new Rock(createBodyObject(BodyDef.BodyType.DynamicBody,x,y,0));
    }

    public void createCircle() {
        Body body = createBodyObject(BodyDef.BodyType.DynamicBody,
                (float) (Math.random()*300)+10,(float) (Math.random()*100)+10,0);
        CircleShape poly = new CircleShape();
        poly.setRadius((float) (Math.random()*3)+3);
        body.createFixture(poly, 3);
        poly.dispose();
        body.setBullet(false);
    }

    public void signObjectЕoListeners(ContactListener... contactListeners) {
        for (ContactListener contactListener:contactListeners) {
            world.setContactListener(contactListener);
        }
    }

    private void createBorderWorld() {
        int kons = 800;
        Body body = createBodyObject(BodyDef.BodyType.StaticBody,0, 0, 0);

        ChainShape chainShape = new ChainShape();
        chainShape.createChain(new Vector2[]{new Vector2(0,0),new Vector2(0,kons),new Vector2(kons,kons),new Vector2(kons,0),new Vector2(0,0)});

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = chainShape;
//        fixtureDef.density = 10;
//        fixtureDef.restitution = 100;
        body.createFixture(fixtureDef);

        chainShape.dispose();
    }

    private Body createBodyObject(BodyDef.BodyType bodyType,float x, float y, float angle) {

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = bodyType;
        Body body = world.createBody(bodyDef);
        body.setTransform(x,y,angle);
        return body;

//        FixtureDef fixtureDef = new FixtureDef();
//
//        PolygonShape shape = new PolygonShape();
//        shape.setAsBox(camera.viewportWidth, 1);
//
//        fixtureDef.shape = shape;
//
//        ground.createFixture(fixtureDef);
//        ground.setTransform(0, 0, 0);
//
//        shape.dispose();
    }

    public void addMarkedObjects(){
        addObject();
    }
    public void removeMarkedObjects(){
        destroyObject();
    }

    public void addObject(IGameObject... iGameObjects){
        for (IGameObject iGameObject:iGameObjects){
            markedForAdd.add(iGameObject);
        }

    }
    public void destroyObject(IGameObject iGameObject){
        markedForDeletion.add(iGameObject);
    }

    private void addObject(){
        if(markedForAdd.size() != 0){
            for (IGameObject iGameObject :markedForAdd){
                if (iGameObject != null) objectStore.addSprite(iGameObject);
            }
            markedForAdd.clear();
        }
    }
    private void destroyObject(){
        if(markedForDeletion.size() != 0){
            for (IGameObject iGameObject :markedForDeletion){
                //System.out.println("Удаляю "+iGameObject);
                if (iGameObject.getClass() == Player.class){System.out.println("Конец игры");}
                if (iGameObject != null) objectStore.remove(iGameObject);
                if (iGameObject.getBody() != null){
                    Body body = iGameObject.getBody();
                    body.setActive(false);
                    for (JointEdge jointEdge:body.getJointList()){
                        world.destroyJoint(jointEdge.joint);
                    }
                    world.destroyBody(body);
                }
            }
            markedForDeletion.clear();
        }
    }
}
